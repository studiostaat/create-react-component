
var path = require('path')

var paths = {}
paths.root = path.resolve(__dirname, '..', '..');
paths.dist = path.resolve(paths.root, 'dist')

module.exports = paths