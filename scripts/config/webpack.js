var webpack = require('webpack');
var path = require('path');
var paths = require('./paths');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = function (type) {
  var config = {
    entry: '',
    output: {
      path: '',
      filename: 'component.js'
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      }]
    },
    plugins: []
  }

  if (type === 'production') {
    config.entry = paths.root + '/src/index.js'
    config.output.path = path.resolve(paths.root, 'dist')
    config.output.library = 'MyComponent'
    config.output.libraryTarget = 'umd'
    config.plugins.push(new UglifyJsPlugin())
    config.externals = {
      "react": "react",
      "react-dom": "react-dom"
    }
  }

  if (type === 'development') {
    config.entry = paths.root + '/scripts/dev-entry/index.js';
    config.output.path = path.resolve(paths.root, 'build')
    config.plugins.push(new webpack.HotModuleReplacementPlugin())
    config.plugins.push(new webpack.NamedModulesPlugin())
  }

  return config;
}
