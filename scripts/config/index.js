
var paths = require('./paths')
var webpack = require('./webpack')

module.exports = { paths: paths, webpack: webpack }