
import React from 'react'
import ReactDOM from 'react-dom'
import Component from '../../src'

const render = (Component) => {
  ReactDOM.render(<Component />, document.querySelector('.root'), () => {
    console.log('Dev-server running...')
  })
}

render(require('../../src').default)

if (module.hot) {
  module.hot.accept('../../src/index.js', () => {
    render(require('../../src').default)
  })
}