var fs = require('fs');

module.exports = function (dirPath) {
  let directory = {}

  directory.exists = function () {
    return fs.existsSync(dirPath)
  }

  directory.remove = function () {
    if (fs.existsSync(dirPath)) {
      fs.readdirSync(dirPath).forEach(function (file, index) {
        var curPath = dirPath + "/" + file;
        if (fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(dirPath);
    }
  }

  directory.create = function () {
    if (!directory.exists()) {
      fs.mkdirSync(dirPath);
    }
  }

  return directory
}
