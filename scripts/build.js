
var webpack = require("webpack");
var config = require('./config')
var dirHelper = require('./helpers/directory')

resetDistDirectory();

var webpackConfig = config.webpack('production')

const compiler = webpack(webpackConfig);

compiler.run(() => {
  
})

function resetDistDirectory () {
  if (dirHelper(config.paths.root + '/dist').exists()) { // remove dist directory
    dirHelper(config.paths.root + '/dist').remove()
  }
  // create dist directory
  dirHelper(config.paths.root + '/dist').create()
}