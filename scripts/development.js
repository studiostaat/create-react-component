
var path = require('path')
var webpack = require("webpack");
var WebpackDevServer = require('webpack-dev-server')
var config = require('./config')

var webpackConfig = config.webpack('development')

const options = {
  contentBase: path.join(config.paths.root, "build"),
  compress: true,
  hot: true,
  host: 'localhost',
  stats: {
    colors: true
  },
  before (app) {
    app.get('/', function(req, res) {
      res.send([
        '<!DOCTYPE HTML>',
        '<html>',
          '<head>',
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />',
            '<title>Your component</title>',
          '</head>',
          '<body>',
            '<div class="root"></div>',
            '<script type="text/javascript" src="component.js"></script>',
          '</body>',
        '</html>'
      ].join(''));
    });
  },
};

WebpackDevServer.addDevServerEntrypoints(webpackConfig, options);
const compiler = webpack(webpackConfig);
const server = new WebpackDevServer(compiler, options);
server.listen(8080, '127.0.0.1', () => {
  console.log('Starting server on http://localhost:8080');
});