// text editor
import React from 'react'
//require('babel-polyfill')

// const Test = async () => {
//   console.log('ok!')
// }

const HOC = Component => props => {
  return <div>
    <h1>Welcome</h1>
    <Component {...props} />
  </div>
}

@HOC
class MyComponent extends React.Component {
  render () {
    return <div>
      <div>
        TEXTEDITOR
      </div>
      <div>
        {this.props.children || null}
      </div>
    </div> 
  }
}

export default props => <MyComponent>
  {props.children}
</MyComponent>